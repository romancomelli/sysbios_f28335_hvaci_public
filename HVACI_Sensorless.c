/****************************************************************************************
 * ╔═╗    ╔═════╗╔═════╗
 * ║ ║    ║ ╔═╗ ║║ ╔═══╝
 * ║ ║    ║ ║ ║ ║║ ║
 * ║ ║    ║ ╚═╝ ║║ ║
 * ║ ╚═══╗║ ╔═╗ ║║ ╚═══╗
 * ╚═════╝╚═╝ ╚═╝╚═════╝
 * 
 * FILE:
 * HVACI_Sensorless.c
 * 
 * DEVICE:
 * TMS320F28335
 * 
 * AUTHOR:
 * Comelli, Roman (rcomelli@fceia.unr.edu.ar, roman.comelli@gmail.com)
 * 
 * DESCRIPTION:
 * Primary system file for the Implementation of a Sensorless Field Oriented Control for
 * Induction Motors (SYS/BIOS version). Supports F2803x (fixed point) and F2833x
 * (floating point).
 ***************************************************************************************/




#define xdc__strict //To avoid warnings because of typedefs.




//Include header files used in the main function.
#include "PeripheralHeaderIncludes.h"
#include "HVACI_Sensorless-Settings.h"
#include "IQmathLib.h"
#include "HVACI_Sensorless.h"
#include "my_f2833xpwm.h"
#include "my_f2833xileg_vdc.h"
#include <math.h>

#include <xdc/std.h>
#include <xdc/runtime/System.h>
#include <xdc/cfg/global.h>
#include <xdc/runtime/Error.h>

#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Swi.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/family/c28/Hwi.h>




/*
#ifdef FLASH
#pragma CODE_SECTION(SwiMainFunction,"ramfuncs");
#pragma CODE_SECTION(SwiOffsetFunction,"ramfuncs");
#endif
*/




//Prototype statements for functions found within this file.
Void SwiMainFunction(UArg arg1, UArg arg2);
Void SwiOffsetFunction(UArg arg1, UArg arg2);
Void ClkTZ(UArg arg);
Void HwiMain(UArg arg);
void DeviceInit();
void MemCopy();
void InitFlash();
void HVDMC_Protection(void);



/*
//Used for running background in flash, and ISR in RAM
extern Uint16 *RamfuncsLoadStart, *RamfuncsLoadEnd, *RamfuncsRunStart;
*/

//Variables used to configure SYS/BIOS.
Swi_Handle swi;
Swi_Params swi_parameters;
Clock_Handle clock;
Clock_Params clock_parameters;
Hwi_Handle hwi;
Hwi_Params hwi_parameters;
Error_Block eb;

//Global variables used in this system.
Uint16 OffsetFlag = 0;
_iq offsetA = 0;
_iq offsetB = 0;
_iq offsetC = 0;
_iq K1 =_IQ(0.998); //Offset filter coefficient K1: 0.05 / (T + 0.05);
_iq K2 =_IQ(0.001999); //Offset filter coefficient K2: T / (T + 0.05);
extern _iq IQsinTable[];
extern _iq IQcosTable[];

_iq VdTesting = _IQ(0.2); //Vd reference (pu).
_iq VqTesting = _IQ(0.2); //Vq reference (pu). 
_iq IdRef = _IQ(0.1); //Id reference (pu).
_iq IqRef = _IQ(0.05); //Iq reference (pu).
_iq SpeedRef = _IQ(0.3); //Speed reference (pu).

float32 T = 0.001 / ISR_FREQUENCY; //Sampling period (sec), see parameter.h.

Uint32 IsrTicker = 0;
Uint16 BackTicker = 0;
Uint16 lsw = 0;
Uint16 TripFlagDMC = 0; //PWM trip status.

//Default ADC initialization.
int ChSel[16] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
int TrigSel[16] = {5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5};
int ACQPS[16] = {8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8};

int16 DlogCh1 = 0;
int16 DlogCh2 = 0;
int16 DlogCh3 = 0;
int16 DlogCh4 = 0;

volatile Uint16 EnableFlag = FALSE;

Uint16 SpeedLoopPrescaler = 10; //Speed loop prescaler.
Uint16 SpeedLoopCount = 1; //Speed loop counter.

//Instance rotor flux and speed estimations.
ACIFE fe1 = ACIFE_DEFAULTS;
ACISE se1 = ACISE_DEFAULTS;

//Instance the constant calculations for rotor flux and speed estimations.
ACIFE_CONST fe1_const = ACIFE_CONST_DEFAULTS; 
ACISE_CONST se1_const = ACISE_CONST_DEFAULTS;

//Instance a QEP interface driver.
QEP qep1 = QEP_DEFAULTS; 

//Instance a Capture interface driver.
CAPTURE cap1 = CAPTURE_DEFAULTS;

//Instance a few transform objects (ICLARKE is added in SVGEN module).
CLARKE clarke1 = CLARKE_DEFAULTS;
PARK park1 = PARK_DEFAULTS;
IPARK ipark1 = IPARK_DEFAULTS;

//Instance PI regulators to regulate the d and q axis currents and speed.
PI_CONTROLLER pi_spd = PI_CONTROLLER_DEFAULTS;
PI_CONTROLLER pi_id = PI_CONTROLLER_DEFAULTS;
PI_CONTROLLER pi_iq = PI_CONTROLLER_DEFAULTS;

//Instance a PWM driver instance.
PWMGEN pwm1 = PWMGEN_DEFAULTS;

//Instance a PWM DAC driver instance.
PWMDAC pwmdac1 = PWMDAC_DEFAULTS;

//Instance a Space Vector PWM modulator. This modulator generates a, b and c phases based
//on the d and q stationery reference frame inputs.
SVGEN svgen1 = SVGEN_DEFAULTS;

//Instance a ramp controller to smoothly ramp the frequency.
RMPCNTL rc1 = RMPCNTL_DEFAULTS;

//Instance a ramp (sawtooth) generator to simulate an Anglele.
RAMPGEN rg1 = RAMPGEN_DEFAULTS;

//Instance a phase voltage calculation.
PHASEVOLTAGE volt1 = PHASEVOLTAGE_DEFAULTS;

//Instance a speed calculator based on QEP.
SPEED_MEAS_QEP speed1 = SPEED_MEAS_QEP_DEFAULTS;

//Instance a speed calculator based on capture Qep (for eQep of 280x only).
SPEED_MEAS_CAP speed2 = SPEED_MEAS_CAP_DEFAULTS;

//Create an instance of DATALOG Module.
DLOG_4CH dlog = DLOG_4CH_DEFAULTS;




void main(void){

    DeviceInit(); //Device Life support & GPIO.

//Only used if running from FLASH.
//Note that the variable FLASH is defined by the compiler.

#ifdef FLASH
//Copy time critical code and Flash setup code to RAM.
//The RamfuncsLoadStart, RamfuncsLoadEnd, and RamfuncsRunStart
//symbols are created by the linker. Refer to the linker files.
    //MemCopy(&RamfuncsLoadStart, &RamfuncsLoadEnd, &RamfuncsRunStart);

//Call Flash Initialization to setup flash waitstates.
//This function must reside in RAM.
    InitFlash(); //Call the flash wrapper init function.
#endif

    System_printf("Ready to start.\n");
    //Waiting for enable flag set.
    while(EnableFlag == FALSE){ 
        BackTicker++;
    }

    //Initialize PWM module.
    pwm1.PeriodMax = SYSTEM_FREQUENCY * 1000000 * T / 2; //Prescaler X1 (T1), ISR period = T x 1.
    pwm1.HalfPerMax = pwm1.PeriodMax / 2;
    pwm1.Deadband = 2.0 * SYSTEM_FREQUENCY; //120 counts -> 2.0 usec for TBCLK = SYSCLK / 1.
    MY_PWM_INIT_MACRO(1, 2, 3, pwm1)

    //Initialize PWMDAC module.
    pwmdac1.PeriodMax = 500; //@60Khz, 1500->20kHz, 1000-> 30kHz, 500->60kHz.
    pwmdac1.HalfPerMax = pwmdac1.PeriodMax / 2;
    PWMDAC_INIT_MACRO(6, pwmdac1) //PWM 6A,6B.
    //PWMDAC_INIT_MACRO(7, pwmdac1) //PWM 7A,7B. //This was removed since there is no PWM7.

    //Initialize DATALOG module.
    dlog.iptr1 = &DlogCh1;
    dlog.iptr2 = &DlogCh2;
    dlog.iptr3 = &DlogCh3;
    dlog.iptr4 = &DlogCh4;
    dlog.trig_value = 0x1;
    dlog.size = 0x0C8;
    dlog.prescalar = 5;
    dlog.init(&dlog);
    
    //Initialize ADC for DMC Kit Rev 1.1.
    ChSel[0] = 1; //Dummy meas. avoid 1st sample issue Rev0 Picollo.
    ChSel[1] = 1; //ChSelect: ADC A1-> Phase A Current.
    ChSel[2] = 9; //ChSelect: ADC B1-> Phase B Current.
    ChSel[3] = 3; //ChSelect: ADC A3-> Phase C Current.
    ChSel[4] = 15; //ChSelect: ADC B7-> Phase A Voltage.
    ChSel[5] = 14; //ChSelect: ADC B6-> Phase B Voltage.
    ChSel[6] = 12; //ChSelect: ADC B4-> Phase C Voltage.
    ChSel[7] = 7; //ChSelect: ADC A7-> DC Bus  Voltage.

    //Initialize ADC module.
    MY_ADC_MACR_INIT(ChSel, TrigSel, ACQPS)

    //Initialize QEP module.
    qep1.LineEncoder = 2048;
    qep1.MechScaler = _IQ30(0.25 / qep1.LineEncoder);
    qep1.PolePairs = POLES / 2;
    qep1.CalibratedAngle = 0;
    QEP_INIT_MACRO(1, qep1)

    //Initialize CAP module.
    CAP_INIT_MACRO(1)

    //Initialize the Speed module for QEP based speed calculation.
    speed1.K1 = _IQ21(1 / (BASE_FREQ * T));
    speed1.K2 = _IQ(1 / (1 + T * 2 * PI * 5));  // Low-pass cut-off frequency
    speed1.K3 = _IQ(1) - speed1.K2;
    speed1.BaseRpm = 120 * (BASE_FREQ / POLES);

    //Initialize the Speed module for capture eQEP based speed calculation (low speed range).
    speed2.InputSelect = 1;
    speed2.BaseRpm = 120 * (BASE_FREQ / POLES);
    speed2.SpeedScaler = 60 * (SYSTEM_FREQUENCY * 1000000 / (1 * 2048 * speed2.BaseRpm));
           
    //Initialize the RAMPGEN module.
    rg1.StepAngleMax = _IQ(BASE_FREQ * T);
    
    //Initialize the aci flux estimator constants module.
    fe1_const.Rs = RS;
    fe1_const.Rr = RR;
    fe1_const.Ls = LS;
    fe1_const.Lr = LR;
    fe1_const.Lm = LM;
    fe1_const.Ib = BASE_CURRENT;
    fe1_const.Vb = BASE_VOLTAGE;
    fe1_const.Ts = T;
    ACIFE_CONST_MACRO(fe1_const)

    //Initialize the aci flux estimator module.
    fe1.K1 = _IQ(fe1_const.K1);
    fe1.K2 = _IQ(fe1_const.K2);
    fe1.K3 = _IQ(fe1_const.K3);
    fe1.K4 = _IQ(fe1_const.K4);
    fe1.K5 = _IQ(fe1_const.K5);
    fe1.K6 = _IQ(fe1_const.K6);
    fe1.K7 = _IQ(fe1_const.K7);
    fe1.K8 = _IQ(fe1_const.K8);
    fe1.Kp = _IQ(2.8); 
    fe1.Ki = _IQ(T/0.45);

    //Initialize the aci speed estimator constants module.
    se1_const.Rr = RR;
    se1_const.Lr = LR;
    se1_const.fb = BASE_FREQ;
    se1_const.fc = 3;
    se1_const.Ts = T;
    ACISE_CONST_MACRO(se1_const)

    //Initialize the aci speed estimator module.
    se1.K1 = _IQ(se1_const.K1);
    se1.K2 = _IQ21(se1_const.K2);
    se1.K3 = _IQ(se1_const.K3);
    se1.K4 = _IQ(se1_const.K4);
    se1.BaseRpm = 120*BASE_FREQ/POLES;

    //Initialize the PI module for speed.
    pi_spd.Kp = _IQ(2.0);
    pi_spd.Ki = _IQ(T * SpeedLoopPrescaler / 0.5);
    pi_spd.Umax = _IQ(0.95);
    pi_spd.Umin = _IQ(-0.95);

    //Initialize the PI module for Id.
    pi_id.Kp = _IQ(1.0);
    pi_id.Ki = _IQ(T / 0.004);
    pi_id.Umax = _IQ(0.3);
    pi_id.Umin = _IQ(-0.3);

    //Initialize the PI module for Iq.
    pi_iq.Kp = _IQ(1.0);
    pi_iq.Ki = _IQ(T / 0.004);
    pi_iq.Umax = _IQ(0.8);
    pi_iq.Umin = _IQ(-0.8);

//Note that the vectorial sum of d-q PI outputs should be less than 1.0 which refers to
//maximum duty cycle for SVGEN. Another duty cycle limiting factor is current sense
//through shunt resistors which depends on hardware/software implementation. Depending on
//the application requirements 3, 2 or a single shunt resistor can be used for current
//waveform reconstruction. The higher number of shunt resistors allow the higher duty
//cycle operation and better dc bus utilization. The users should adjust the PI
//saturation levels carefully during open loop tests (i.e pi_id.Umax, pi_iq.Umax and
//Umins) as in project manuals. Violation of this procedure yields distorted current
//waveforms and unstable closed loop operations which may damage the inverter. 

    //Call HVDMC Protection function.
    HVDMC_Protection();

    Error_init(&eb);

    Swi_Params_init(&swi_parameters);
    swi_parameters.priority = 2; //Higher than the priority of the swi posted by the clock module.
    swi = Swi_create(SwiOffsetFunction, &swi_parameters, &eb);
    if(swi == NULL){
        System_abort("Swi creation failed.\n");
    }

    Clock_Params_init(&clock_parameters);
    clock_parameters.period = 1;
    clock_parameters.startFlag = TRUE;
    clock = Clock_create(ClkTZ, 1, &clock_parameters, &eb);
    if (clock == NULL) {
        System_abort("Clock creation failed.\n");
    }

    Hwi_Params_init(&hwi_parameters);
    hwi_parameters.enableAck = TRUE;
    hwi_parameters.enableInt = TRUE;
    hwi_parameters.maskSetting = Hwi_MaskingOption_ALL;
    hwi = Hwi_create(37, HwiMain, &hwi_parameters, &eb);
    if (hwi == NULL) {
        System_abort("Hwi creation failed.\n");
    }

    BIOS_start();
}



Void HwiMain(UArg arg){
    Swi_post(swi);
    AdcRegs.ADCST.bit.INT_SEQ1_CLR = 1; //Clear INT SEQ1 bit.
}



//ClkTZ is what TASKS are in the non-SYS/BIOS example.
Void ClkTZ(UArg arg){
    //Trip on DMC (halt and IPM fault trip).
    if(EPwm1Regs.TZFLG.bit.OST==0x1) TripFlagDMC=1;
}


#pragma CODE_SECTION(SwiMainFunction, ".TI.ramfunc");
//SwiMainFunction is what MainISR is in the non-SYS/BIOS example.
Void SwiMainFunction(UArg arg1, UArg arg2){

    //Verifying the ISR.
    IsrTicker++;

//Level 6 verifies the speed regulator performed by PI module. The system speed loop is
//closed by using the estimated speed as a feedback. lsw = 0 means to close the current
//loop and lsw = 1 means to close the speed loop.

#if (BUILDLEVEL==LEVEL6)

    //Connect inputs of the RMP module and call the ramp control macro.
    rc1.TargetValue = SpeedRef;        
    RC_MACRO(rc1)

    //Connect inputs of the RAMP GEN module and call the ramp generator macro
    rg1.Freq = rc1.SetpointValue;
    RG_MACRO(rg1)

//Measure phase currents, subtract the offset and normalize from (-0.5,+0.5) to (-1,+1).
//Connect inputs of the CLARKE module and call the clarke transformation macro.
    #ifdef DSP2833x_DEVICE_H
    //((ADCmeas(q12) / 2^12) - offset) * 2 * (3.0 / 3.3)
    clarke1.As = ((AdcMirror.ADCRESULT1) * 0.00024414 - offsetA) * 1.81818; //Phase A curr.
    clarke1.Bs = ((AdcMirror.ADCRESULT2) * 0.00024414 - offsetB) * 1.81818; //Phase B curr.
    #endif

    #ifdef DSP2803x_DEVICE_H
    //(ADCmeas(q12->q24) - offset) * 2
    clarke1.As = _IQmpy2(_IQ12toIQ(AdcResult.ADCRESULT1) - offsetA); //Phase A curr.
    clarke1.Bs = _IQmpy2(_IQ12toIQ(AdcResult.ADCRESULT2) - offsetB); //Phase B curr.
    #endif

    CLARKE_MACRO(clarke1)  

    //Connect inputs of the PARK module and call the park trans. macro.
    park1.Alpha = clarke1.Alpha;
    park1.Beta = clarke1.Beta;
    if(lsw == 0) park1.Angle = rg1.Out;
    else park1.Angle = fe1.ThetaFlux;
    park1.Sine = _IQsinPU(park1.Angle);
    park1.Cosine = _IQcosPU(park1.Angle);
    PARK_MACRO(park1)
 
    //Connect inputs of the PI module and call the PI SPD controller macro.
    if(SpeedLoopCount == SpeedLoopPrescaler){
        pi_spd.Ref = rc1.SetpointValue;
        pi_spd.Fbk = se1.WrHat;
        PI_MACRO(pi_spd)
        SpeedLoopCount=1;
    }
    else SpeedLoopCount++;  

    if(lsw==0){
        pi_spd.ui = 0;
        pi_spd.i1 = 0;
    }

    //Connect inputs of the PI module and call the PI ID controller macro.
    if(lsw == 0) pi_iq.Ref = IqRef;
    else pi_iq.Ref =  pi_spd.Out;
    pi_iq.Fbk = park1.Qs;
    PI_MACRO(pi_iq)

    //Connect inputs of the PI module and call the PI ID controller macro.
    pi_id.Ref = IdRef;
    pi_id.Fbk = park1.Ds;
    PI_MACRO(pi_id)

    //Connect inputs of the INV_PARK module and call the inverse park trans. macro.
    ipark1.Ds = pi_id.Out;
    ipark1.Qs = pi_iq.Out ;
    ipark1.Sine = park1.Sine;
    ipark1.Cosine = park1.Cosine;
    IPARK_MACRO(ipark1)

//Call the QEP macro (if incremental encoder used for speed sensing). Connect inputs of
//the SPEED_FR module and call the speed calculation macro.
    QEP_MACRO(1,qep1)

    speed1.ElecTheta = qep1.ElecTheta;
    speed1.DirectionQep = (int32)(qep1.DirectionQep);
    SPEED_FR_MACRO(speed1) 


    //Connect inputs of the VOLT_CALC module and call the phase voltage calc. macro.
    #ifdef DSP2833x_DEVICE_H
    //(ADCmeas(q12) / 2^12) * (3.0 /3.3)
    volt1.DcBusVolt = ((AdcMirror.ADCRESULT7) * 0.00024414) * 0.909; //DC Bus voltage measure.
    #endif
    
    #ifdef DSP2803x_DEVICE_H
    volt1.DcBusVolt = _IQ12toIQ(AdcResult.ADCRESULT7); //DC Bus voltage meas.
    #endif
    
    volt1.MfuncV1 = svgen1.Ta;
    volt1.MfuncV2 = svgen1.Tb;
    volt1.MfuncV3 = svgen1.Tc;
    PHASEVOLT_MACRO(volt1)        

    //Connect inputs of the ACI module and call the flux estimation macro.
    fe1.UDsS = volt1.Valpha;
    fe1.UQsS = volt1.Vbeta;
    fe1.IDsS = clarke1.Alpha;
    fe1.IQsS = clarke1.Beta;
    ACIFE_MACRO(fe1)

    //Connect inputs of the ACI module and call the speed estimation macro.
    se1.IDsS = clarke1.Alpha;
    se1.IQsS = clarke1.Beta;
    se1.PsiDrS = fe1.PsiDrS;
    se1.PsiQrS = fe1.PsiQrS;
    se1.ThetaFlux = fe1.ThetaFlux; 
    ACISE_MACRO(se1)

    //Connect inputs of the SVGEN_DQ module and call the space-vector gen. macro.
    svgen1.Ualpha = ipark1.Alpha;
    svgen1.Ubeta = ipark1.Beta;
    SVGENDQ_MACRO(svgen1)        

    //Connect inputs of the PWM_DRV module and call the PWM signal generation macro.
    pwm1.MfuncC1 = svgen1.Ta;
    pwm1.MfuncC2 = svgen1.Tb;
    pwm1.MfuncC3 = svgen1.Tc;
    PWM_MACRO(1,2,3,pwm1) //Calculate the new PWM compare values.

    //Connect inputs of the PWMDAC module.
    pwmdac1.MfuncC1 = speed1.Speed; 
    pwmdac1.MfuncC2 = se1.WrHat; 
    PWMDAC_MACRO(6, pwmdac1) //PWMDAC 6A, 6B.
    
    //This was removed since there is no PWM7.
    //pwmdac1.MfuncC1 = rg1.Out; 
    //pwmdac1.MfuncC2 = fe1.ThetaFlux; 
    //PWMDAC_MACRO(7, pwmdac1) //PWMDAC 7A, 7B.

    //Connect inputs of the DATALOG module.
    DlogCh1 = _IQtoQ15(clarke1.Bs);
    DlogCh2 = _IQtoQ15(clarke1.As);
    DlogCh3 = _IQtoQ15(fe1.ThetaFlux);
    DlogCh4 = _IQtoQ15(volt1.VphaseA);

#endif //(BUILDLEVEL==LEVEL6) 


    //Call the DATALOG update function.
    dlog.update(&dlog);

} //SwiMainFunction ends here.


//SwiOffsetFunction is what OffsetISR (Offset Compensation) is in the non-SYS/BIOS example.
Void SwiOffsetFunction(UArg arg1, UArg arg2){
    //Verifying the ISR.
    IsrTicker++;

    //DC offset measurement for ADC.
    if(IsrTicker >= 5000){
        #ifdef DSP2833x_DEVICE_H
        offsetA = K1 * offsetA + K2 * (AdcMirror.ADCRESULT1) * 0.00024414; //Phase A offset.
        offsetB = K1 * offsetB + K2 * (AdcMirror.ADCRESULT2) * 0.00024414; //Phase B offset.
        offsetC = K1 * offsetC + K2 * (AdcMirror.ADCRESULT3) * 0.00024414; //Phase C offset.
        #endif
        
        #ifdef DSP2803x_DEVICE_H
        offsetA = _IQmpy(K1, offsetA) + _IQmpy(K2, _IQ12toIQ(AdcResult.ADCRESULT1)); //Phase A offset.
        offsetB = _IQmpy(K1, offsetB) + _IQmpy(K2, _IQ12toIQ(AdcResult.ADCRESULT2)); //Phase B offset.
        offsetC = _IQmpy(K1, offsetC) + _IQmpy(K2, _IQ12toIQ(AdcResult.ADCRESULT3)); //Phase C offset.
        #endif
    }

    if (IsrTicker > 20000){
        /*
        EALLOW;
        PieVectTable.EPWM1_INT = &SwiMainFunction;
        EDIS;
        */
        Swi_setAttrs(swi, SwiMainFunction, NULL);
    }
}


//Protection Configuration.
void HVDMC_Protection(void){

    EALLOW;

//Configure Trip Mechanism for the Motor control software:
//-Cycle by cycle trip on CPU halt.
//-One shot IPM trip zone trip.
//These trips need to be repeated for EPWM1, 2 and 3.

    //CPU Halt Trip.
    EPwm1Regs.TZSEL.bit.CBC6 = 0x1;
    EPwm2Regs.TZSEL.bit.CBC6 = 0x1;
    EPwm3Regs.TZSEL.bit.CBC6 = 0x1;

    EPwm1Regs.TZSEL.bit.OSHT1 = 1; //Enable TZ1 for OSHT.
    EPwm2Regs.TZSEL.bit.OSHT1 = 1; //Enable TZ1 for OSHT.
    EPwm3Regs.TZSEL.bit.OSHT1 = 1; //Enable TZ1 for OSHT.

//What do we want the OST/CBC events to do?
//TZA events can force EPWMxA.
//TZB events can force EPWMxB.

    EPwm1Regs.TZCTL.bit.TZA = TZ_FORCE_LO; //EPWMxA will go low.
    EPwm1Regs.TZCTL.bit.TZB = TZ_FORCE_LO; //EPWMxB will go low.

    EPwm2Regs.TZCTL.bit.TZA = TZ_FORCE_LO; //EPWMxA will go low.
    EPwm2Regs.TZCTL.bit.TZB = TZ_FORCE_LO; //EPWMxB will go low.

    EPwm3Regs.TZCTL.bit.TZA = TZ_FORCE_LO; //EPWMxA will go low.
    EPwm3Regs.TZCTL.bit.TZB = TZ_FORCE_LO; //EPWMxB will go low.

    EDIS;

    //Clear any spurious OV trip.
    EPwm1Regs.TZCLR.bit.OST = 1;
    EPwm2Regs.TZCLR.bit.OST = 1;
    EPwm3Regs.TZCLR.bit.OST = 1;  

}

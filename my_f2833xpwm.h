#ifndef __MY_F2833X_PWM_H__
#define __MY_F2833X_PWM_H__

#if (DSP2803x_DEVICE_H==1)
#include "f2803xpwm.h"        	// Include header for the PWMGEN object
#endif

#if (DSP2833x_DEVICE_H==1)
#include "f2833xpwm.h"			// Include header for the PWMGEN object
#endif

#define MY_PWM_INIT_MACRO(ch1,ch2,ch3,v)	    							\
                                                                            \
    PWM_INIT_MACRO(ch1,ch2,ch3,v)                                           \
                                                                            \
/*Added to make timers stop while the system is halted in debugging.*/      \
/*These lines modify things done in PWM INIT MACRO. It is done this way*/   \
/*to avoid changes in the file containing that definition.*/                \
    (*ePWM[1]).TBCTL.bit.FREE_SOFT = 0;                                     \
    (*ePWM[2]).TBCTL.bit.FREE_SOFT = 0;                                     \
    (*ePWM[3]).TBCTL.bit.FREE_SOFT = 0;

#endif  // __MY_F2833X_PWM_H__

Los cambios para bajar el programa a RAM en lugar de a FLASH, que se hicieron a partir
del commit siguiente al 6f2297a fueron:
-Excluir el archivo F28335_FLASH_HVACI_Sensorless.cmd (habiendo incluido el
F28335_RAM_HVACI_Sensorless.CMD).
-Borrar de los Predefined Symbols (en las propiedades del proyecto en la parte del
compilador), el símbolo "FLASH".

Tener en cuenta además las posiciones de los switches de la ControlCard. Si bien en
esta placa no hace diferencia (arranca igual a diferencia de la F28M35 por ejemplo),
se supone que si corremos el programa desde RAM tenemos que ponerlos para que bootee
desde allí. Y si queremos volver a cargar el programa en FLASH, hay que moverlos
nuevamente (además de modificar las cosas indicadas en los dos ítems anteriores).

Tener en cuenta también que la ejecución del programa desde RAM no trae grandes
diferencias en términos de velocidad de ejecución respecto de correrlo desde FLASH
(siempre y cuando comparemos RAM interna contra FLASH interna y ésta última esté
correctamente configurada con los waitstates que correspondan y tenga el pipelining
activado). En este thread
https://e2e.ti.com/support/microcontrollers/c2000/f/171/t/264130 hay información
oficial sobre las diferencias en las velocidades de procesamiento. Sí se nota igual
una diferencia. Por ejemplo, TBCTR al momento de comenzarse a ejecutar la swi del
control está en 6070 aproximadamente cuando el programa corre desde RAM mientras que
cuando lo hace desde FLASH está en alrededor de 6000 (baja desde 7500 hasta 0 por lo
que es más rápido correr desde RAM pero no hace una gran diferencia.)

Es probable que sea necesario modificar el archivo F28335_RAM_HVACI_Sensorless.CMD
para poder correr el programa, en caso de que crezca la sección .text o se quiera
incrementar la stack, por ejemplo. Podría ocurrir que el programa no quepa aunque
estimo que no va a pasar.

Una ventaja de correr el programa desde RAM es que se pueden poner muchos más
breakpoints que cuando se ejecuta desde FLASH. Y además, los breakpoints andan mejor.
Cuando se corre desde FLASH, muchas veces el programa se detiene a la primera vez que
pasa por el breakpoint y luego no más. Esto no pasa cuando se ejecuta desde RAM.